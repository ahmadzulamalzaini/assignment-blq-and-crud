﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PERPUSTAKAAN.DataModel
{
    [Table("Rak")]
    public partial class Rak
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("nama_rak")]
        [StringLength(50)]
        [Unicode(false)]
        public string NamaRak { get; set; } = null!;
        [Column("lokasi_rak")]
        [StringLength(50)]
        [Unicode(false)]
        public string LokasiRak { get; set; } = null!;
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }
        [Column("deleted_by")]
        public int? DeletedBy { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Column("create_by")]
        public int CreateBy { get; set; }
        [Column("create_date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("update_by")]
        public int? UpdateBy { get; set; }
        [Column("update_date", TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
    }
}
