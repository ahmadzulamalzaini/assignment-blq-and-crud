﻿using Newtonsoft.Json;
using PERPUSTAKAAN.ViewModel;
using System.Net;
using System.Text;

namespace PERPUSTAKAAN.Models
{
    public class RakModel
    {
        private string? jsonData;
        private HttpContent? content;
        private VMResponse? apiResponse;
        private readonly string? apiUrl;
        private readonly HttpClient httpClient;

        public RakModel(IConfiguration _config)
        {
            apiUrl = _config["ApiUrl"];
            httpClient = new HttpClient();
        }
        public async Task<VMResponse> GetAll()
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await
                    httpClient.GetStringAsync(apiUrl + "api/Rak"));
                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK || apiResponse.statusCode ==
                        HttpStatusCode.NoContent)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<
                            List<VMRak>>(apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("tidak bisa diakses");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message = $" {ex.Message}";
                apiResponse.data = new List<VMRak>();

            }
            return apiResponse;
        }
        public async Task<VMResponse> GetById(int id)
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await
                    httpClient.GetStringAsync(apiUrl + $"api/Rak/GetById/?id={id}"));
                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK || apiResponse.statusCode ==
                        HttpStatusCode.NoContent)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<
                            VMRak>(apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("tidak bisa diakses");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message = $" {ex.Message}";
                apiResponse.data = new List<VMRak>();

            }
            return apiResponse;
        }
        public async Task<VMResponse> Create(VMRak data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>
                    (await (await httpClient.PostAsync(apiUrl + "api/Rak", content)).Content.ReadAsStringAsync());

                if (apiResponse == null)
                {
                    throw new Exception("API cannot be reached");
                }

            }
            catch (Exception ex)
            {
                apiResponse.message += $"  {ex.Message}";
            }
            return apiResponse;
        }
        public async Task<VMResponse> Update(VMRak data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>
                    (await (await httpClient.PutAsync(apiUrl + "api/Rak", content)).Content.ReadAsStringAsync());

                if (apiResponse == null)
                {
                    throw new Exception("API cannot be reached");
                }

            }
            catch (Exception ex)
            {
                apiResponse.message += $"  {ex.Message}";
            }
            return apiResponse;
        }
        public async Task<VMResponse> Delete(int id, int idPengguna)
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>
                    (await (await httpClient.DeleteAsync
                    (apiUrl + $"api/Rak/?id={id}&userId={idPengguna}")).Content.ReadAsStringAsync());

                if (apiResponse == null) throw new Exception("Hapus Lokasi Tidak Bisa Dilakukan!");
            }
            catch (Exception ex)
            {
                apiResponse.message += $" {ex.Message}";
            }
            return apiResponse;
        }

    }
}
