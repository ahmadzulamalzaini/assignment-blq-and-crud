﻿using PERPUSTAKAAN.DataModel;
using PERPUSTAKAAN.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PERPUSTAKAAN.DataAccess
{
    public class DABuku
    {
        private readonly PERPUSTAKAANContext db;
        public DABuku(PERPUSTAKAANContext _db)
        {
            db = _db;
        }
        private VMResponse response = new VMResponse();

        public VMResponse GetAll()
        {
            try
            {
                List<VMBuku> data = (
                    from b in db.Bukus
                    join r in db.Raks
                    on b.IdRak equals r.Id
                    where b.IsDeleted == false
                    select new VMBuku
                    {
                        Id = b.Id,
                        IdRak = r.Id,
                        KodeBuku = b.KodeBuku,
                        JudulBuku = b.JudulBuku,
                        PenulisBuku = b.PenulisBuku,
                        PenerbitBuku = b.PenerbitBuku,
                        TahunPenerbit = b.TahunPenerbit,
                        NamaGambar = b.NamaGambar,
                        FileGambar = b.FileGambar,
                        Stok = b.Stok,

                        IsDeleted = b.IsDeleted,
                        DeletedDate = b.DeletedDate,
                        CreateBy = b.CreateBy,
                        CreateDate = b.CreateDate,
                        UpdateBy = b.UpdateBy,
                        UpdateDate = b.UpdateDate,
                    }
                ).ToList();

                response.data = data;
                response.message = (data.Count < 1) ? "Data buku tidak ada" : "Data buku ditemukan";
                response.statusCode = (data.Count < 1) ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }
        public VMBuku? FindById(int id)
        {
            return (
                from b in db.Bukus
                join r in db.Raks
                on b.IdRak equals r.Id
                where b.IsDeleted == false && b.Id == id
                select new VMBuku
                {
                    Id = b.Id,
                    IdRak = r.Id,
                    KodeBuku = b.KodeBuku,
                    JudulBuku = b.JudulBuku,
                    PenulisBuku = b.PenulisBuku,
                    PenerbitBuku = b.PenerbitBuku,
                    TahunPenerbit = b.TahunPenerbit,
                    NamaGambar = b.NamaGambar,
                    FileGambar = b.FileGambar,
                    Stok = b.Stok,

                    IsDeleted = b.IsDeleted,
                    DeletedDate = b.DeletedDate,
                    CreateBy = b.CreateBy,
                    CreateDate = b.CreateDate,
                    UpdateBy = b.UpdateBy,
                    UpdateDate = b.UpdateDate,
                }
            ).FirstOrDefault();
        }
        public VMResponse GetById(int id)
        {
            try
            {
                VMBuku? data = FindById(id);
                response.data = data;
                response.message = (data == null)
                    ? $"Buku dengan id = {id} tidak ditemukan"
                    : $"Buku dengan id = {id} berhasil ditemukan";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }
        public List<VMBuku>? FindByRakId(int id)
        {
            return (
                from b in db.Bukus
                join r in db.Raks
                on b.IdRak equals r.Id
                where b.IsDeleted == false && b.IdRak == id
                select new VMBuku
                {
                    Id = b.Id,
                    IdRak = r.Id,
                    KodeBuku = b.KodeBuku,
                    JudulBuku = b.JudulBuku,
                    PenulisBuku = b.PenulisBuku,
                    PenerbitBuku = b.PenerbitBuku,
                    TahunPenerbit = b.TahunPenerbit,
                    NamaGambar = b.NamaGambar,
                    FileGambar = b.FileGambar,
                    Stok = b.Stok,

                    IsDeleted = b.IsDeleted,
                    DeletedDate = b.DeletedDate,
                    CreateBy = b.CreateBy,
                    CreateDate = b.CreateDate,
                    UpdateBy = b.UpdateBy,
                    UpdateDate = b.UpdateDate,
                }
            ).ToList();
        }
        public VMResponse GetByRakId(int id)
        {
            try
            {
                List<VMBuku>? data = FindByRakId(id);
                response.data = data;
                response.message = (data == null)
                    ? $"Buku dengan Rak id = {id} tidak ditemukan"
                    : $"Buku dengan Rak id = {id} berhasil ditemukan";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }
        private string CodeGenerator(int? lastId)
        {
            string prefix = $"Buku-";
            string trxNo = (lastId != null) ? (lastId + 1).ToString().PadLeft(3, '0') : "00000";

            return $"{prefix}{trxNo}";
        }
        public VMResponse Create(VMBuku dataInput)
        {
            try
            {
                Buku data = new Buku();
                data.IdRak = dataInput.IdRak;
                data.KodeBuku = CodeGenerator(
                            db.Bukus.OrderByDescending(
                                oh => oh.Id).Select(oh => oh.Id).FirstOrDefault());
                data.JudulBuku = dataInput.JudulBuku;
                data.PenulisBuku = dataInput.PenulisBuku;
                data.PenerbitBuku = dataInput.PenerbitBuku;
                data.TahunPenerbit = dataInput.TahunPenerbit;
                data.NamaGambar = dataInput.NamaGambar;
                data.FileGambar = dataInput.FileGambar;
                data.Stok = dataInput.Stok;

                data.IsDeleted = false;
                data.CreateBy = dataInput.CreateBy;
                data.CreateDate = DateTime.Now;

                //prosess create new data in table from data model
                db.Add(data);
                db.SaveChanges();

                //update api response
                response.data = data;
                response.message = "Penambahan Buku Berhasil";
                response.statusCode = HttpStatusCode.Created;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }
        public VMResponse Update(VMBuku dataInput)
        {
            try
            {
                VMBuku? dataExist = FindById(dataInput.Id);

                if (dataExist != null)
                {
                    Buku data = new Buku();
                    data.Id = dataExist.Id;
                    data.IdRak = dataInput.IdRak;
                    data.KodeBuku = dataExist.KodeBuku;
                    data.JudulBuku = dataInput.JudulBuku;
                    data.PenulisBuku = dataInput.PenulisBuku;
                    data.PenerbitBuku = dataInput.PenerbitBuku;
                    data.TahunPenerbit = dataInput.TahunPenerbit;
                    data.NamaGambar = dataInput.NamaGambar;
                    data.FileGambar = dataInput.FileGambar;
                    data.Stok = dataInput.Stok;

                    data.IsDeleted = false;
                    data.CreateBy = dataExist.Id;
                    data.CreateDate = dataExist.CreateDate;

                    data.UpdateBy = dataInput.UpdateBy;
                    data.UpdateDate = DateTime.Now;

                    db.Update(data);
                    db.SaveChanges();

                    //update api response
                    response.data = data;
                    response.message = "Berhasil menambahkan buku baru";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    throw new Exception(response.message);
                }


            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }

            return response;
        }
        public VMResponse Delete(int id, int userId)
        {
            try
            {
                VMBuku? dataExist = FindById(id);
                if (dataExist != null)
                {
                    Buku data = new Buku();
                    data.Id = dataExist.Id;
                    data.IdRak = dataExist.IdRak;
                    data.KodeBuku = dataExist.KodeBuku;
                    data.JudulBuku = dataExist.JudulBuku;
                    data.PenulisBuku = dataExist.PenulisBuku;
                    data.PenerbitBuku = dataExist.PenerbitBuku;
                    data.TahunPenerbit = dataExist.TahunPenerbit;
                    data.NamaGambar = dataExist.NamaGambar;
                    data.FileGambar = dataExist.FileGambar;
                    data.Stok = dataExist.Stok;

                    data.IsDeleted = true;
                    data.DeletedBy = userId;
                    data.DeletedDate = DateTime.Now;
                    data.CreateBy = dataExist.Id;
                    data.CreateDate = dataExist.CreateDate;

                    data.UpdateBy = dataExist.UpdateBy;
                    data.UpdateDate = dataExist.UpdateDate;


                    db.Update(data);
                    db.SaveChanges();
                    response.data = data;

                    response.message = ($"Delete berhasil");
                    response.statusCode = HttpStatusCode.OK;
                }
            }
            catch
            {
                response.message = $"Delete gagal";
            }

            return response;
        }
    }
}
