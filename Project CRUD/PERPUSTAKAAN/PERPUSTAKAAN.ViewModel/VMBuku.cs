﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace PERPUSTAKAAN.ViewModel
{
    public class VMBuku
    {
        public int Id { get; set; }
        public int IdRak { get; set; }
        public string KodeBuku { get; set; } = null!;
        public string JudulBuku { get; set; } = null!;
        public string PenulisBuku { get; set; } = null!;
        public string PenerbitBuku { get; set; } = null!;
        public string TahunPenerbit { get; set; } = null!;
        public string? NamaGambar { get; set; }
        public byte[]? FileGambar { get; set; }
        public IFormFile? imageFile { get; set; }
        public int Stok { get; set; }
        public bool IsDeleted { get; set; }
        public int? DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        [NotMapped]
        public string? ImageBase64 { get; set; }
    }
}