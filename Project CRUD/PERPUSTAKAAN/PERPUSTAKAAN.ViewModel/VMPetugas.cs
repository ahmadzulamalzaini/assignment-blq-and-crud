﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PERPUSTAKAAN.ViewModel
{
    public class VMPetugas
    {
        public int Id { get; set; }
        public string NamaPetugas { get; set; } = null!;
        public string JabatanPetugas { get; set; } = null!;
        public string NoTelpPetugas { get; set; } = null!;
        public string AlamatPetugas { get; set; } = null!;
        public bool IsDeleted { get; set; }
        public int? DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
