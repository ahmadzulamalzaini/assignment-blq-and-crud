﻿using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Mvc;
using PERPUSTAKAAN.Models;
using PERPUSTAKAAN.ViewModel;
using System.Net;

namespace PERPUSTAKAAN.Controllers
{
    public class BukuController : Controller
    {
        private VMResponse? response;
        private RakModel rak;
        private BukuModel buku;
        string JsonData;
        private readonly string imageFolder;
        string imageSrc;
        string imageBase64;
        public BukuController(IConfiguration _config, IWebHostEnvironment _webHostEnv)
        {
            rak = new RakModel(_config);
            buku = new BukuModel(_config);
            imageFolder = _config["ImageFolder"];
        }
        public async Task<IActionResult> Index(int id)
        {
            response = await buku.GetByRakId(id);
            List<VMBuku> dataBuku = (List<VMBuku>)response.data;
            foreach(VMBuku data in dataBuku)
            {
                byte[] gambar = data.FileGambar;
                if (gambar != null && gambar.Length > 0)
                {
                    imageBase64 = Convert.ToBase64String(gambar);
                    imageSrc = $"data:image/jpeg;base64,{imageBase64}";
                    data.ImageBase64 = imageSrc;
                }
            }
            
            VMResponse rakResponse = await rak.GetById(id);
            ViewBag.Rak = ((VMRak)rakResponse.data).NamaRak;
            ViewBag.Buku = dataBuku;
            ViewBag.RakId = id;
            ViewBag.imgFolder = imageFolder;

            return View();
        }
        public IActionResult Tambah(int idRak)
        {
            ViewBag.RakId = idRak;
            return View();
        }
        public async Task<bool> CekBuku(string JudulBuku)
        {
            bool result = false;
            int x = 0;
            VMResponse bukuResponse = await buku.GetAll();
            List<VMBuku> dataBuku = (List<VMBuku>)bukuResponse.data;
            foreach (VMBuku data in dataBuku)
            {
                if ((data.JudulBuku).ToLower() == JudulBuku.ToLower())
                {
                    x = x + 1;
                }
            }
            if (x == 0)
            {
                result = true;
            }

            return result;
        }
        public async Task<IActionResult> KonTambah(VMBuku data)
        {
            if (data.imageFile != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    await data.imageFile.CopyToAsync(ms);
                    data.FileGambar = ms.ToArray();
                }
                
                byte[] gambar = data.FileGambar;
                if (gambar != null && gambar.Length > 0)
                {
                    imageBase64 = Convert.ToBase64String(gambar);
                    imageSrc = $"data:image/jpeg;base64,{imageBase64}";
                    data.ImageBase64 = imageSrc;
                }
            }
            ViewBag.Data = data;
            ViewBag.imgFolder = imageFolder;
            return View();
        }
        [HttpPost]
        public async Task<VMResponse> Tambah(VMBuku data, int craeteBy)
        {
            data.CreateBy = craeteBy;
            response = await buku.Create(data);
            ViewBag.Data = data;
            ViewBag.imgFolder = imageFolder;
            if (response.statusCode == HttpStatusCode.Created)
            {
                HttpContext.Session.SetString("infoMsg", "Penambahan buku baru berhasil");
            }
            else
            {
                HttpContext.Session.SetString("errMsg", "Penambahan buku gagal");
            }
            return response;
        }
        public async Task<IActionResult> Edit(int id)
        {
            response = await buku.GetById(id);
            ViewBag.Data = (VMBuku)response.data;
            return View();
        }
        public async Task<bool> CekNamaBuku(string JudulBuku, string nama)
        {
            bool result = false;
            int x = 0;
            VMResponse bukuResponse = await buku.GetAll();
            List<VMBuku> dataBuku = (List<VMBuku>)bukuResponse.data;
            foreach (VMBuku data in dataBuku)
            {
                if ((data.JudulBuku).ToLower() == JudulBuku.ToLower())
                {
                    if (nama.ToLower() != data.JudulBuku.ToLower())
                    {
                        x = x + 1;
                    }
                }
            }
            if (x == 0)
            {
                result = true;
            }

            return result;
        }
        public async Task<IActionResult> KonEdit(VMBuku data)
        {
            if (data.imageFile != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    await data.imageFile.CopyToAsync(ms);
                    data.FileGambar = ms.ToArray();
                }

                byte[] gambar = data.FileGambar;
                if (gambar != null && gambar.Length > 0)
                {
                    imageBase64 = Convert.ToBase64String(gambar);
                    imageSrc = $"data:image/jpeg;base64,{imageBase64}";
                    data.ImageBase64 = imageSrc;
                }
            }
            else
            {
                VMResponse bukuResponse = await buku.GetById(data.Id);
                VMBuku dataBuku = (VMBuku)bukuResponse.data;
                data.FileGambar = dataBuku.FileGambar;
                byte[] gambar = data.FileGambar;
                if (gambar != null && gambar.Length > 0)
                {
                    using (MemoryStream ms = new MemoryStream(gambar))
                    {
                        imageBase64 = Convert.ToBase64String(gambar);
                        imageSrc = $"data:image/jpeg;base64,{imageBase64}";
                        data.ImageBase64 = imageSrc;
                    }
                }
            }
            ViewBag.Data = data;
            ViewBag.imgFolder = imageFolder;
            return View();
        }
        [HttpPost]
        public async Task<VMResponse> Edit(VMBuku data)
        {
            response = await buku.Update(data);
            ViewBag.Data = data;
            ViewBag.imgFolder = imageFolder;
            if (response.statusCode == HttpStatusCode.OK)
            {
                HttpContext.Session.SetString("infoMsg", "Proses edit berhasil");
            }
            else
            {
                HttpContext.Session.SetString("errMsg", "Proses edit gagal");
            }
            return response;
        }
        public async Task<IActionResult> Hapus(int id)
        {
            response = await buku.GetById(id);
            ViewBag.Id = id;
            ViewBag.Nama = ((VMBuku)response.data).JudulBuku;
            ViewBag.IdRak = ((VMBuku)response.data).IdRak;
            return View();
        }

        [HttpPost]
        public async Task<VMResponse> Hapus(int id, int idPengguna)
        {
            response = await buku.Delete(id, idPengguna);
            if (response.statusCode == HttpStatusCode.OK)
            {
                HttpContext.Session.SetString("infoMsg", "Hapus buku berhasil");
            }
            else
            {
                HttpContext.Session.SetString("errMsg", "Hapus buku gagal");
            }
            return response;
        }
    }
}
