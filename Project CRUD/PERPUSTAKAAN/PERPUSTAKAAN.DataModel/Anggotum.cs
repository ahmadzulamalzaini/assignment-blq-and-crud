﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PERPUSTAKAAN.DataModel
{
    public partial class Anggotum
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("kode_anggota")]
        [StringLength(10)]
        [Unicode(false)]
        public string KodeAnggota { get; set; } = null!;
        [Column("nama_anggota")]
        [StringLength(100)]
        [Unicode(false)]
        public string NamaAnggota { get; set; } = null!;
        [Column("jk_anggota")]
        [StringLength(1)]
        [Unicode(false)]
        public string JkAnggota { get; set; } = null!;
        [Column("jurusan_anggota")]
        [StringLength(50)]
        [Unicode(false)]
        public string JurusanAnggota { get; set; } = null!;
        [Column("no_telp_anggota")]
        [StringLength(13)]
        [Unicode(false)]
        public string NoTelpAnggota { get; set; } = null!;
        [Column("alamat_anggota")]
        [StringLength(100)]
        [Unicode(false)]
        public string AlamatAnggota { get; set; } = null!;
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }
        [Column("deleted_by")]
        public int? DeletedBy { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Column("create_by")]
        public int CreateBy { get; set; }
        [Column("create_date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("update_by")]
        public int? UpdateBy { get; set; }
        [Column("update_date", TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
    }
}
