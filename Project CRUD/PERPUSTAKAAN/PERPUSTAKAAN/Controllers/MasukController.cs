﻿using Microsoft.AspNetCore.Mvc;
using PERPUSTAKAAN.Models;
using PERPUSTAKAAN.ViewModel;
using System.Net;

namespace PERPUSTAKAAN.Controllers
{
	public class MasukController : Controller
	{
		private MasukModel masuk;
		private VMResponse? response;
		string JsonData;
		public MasukController(IConfiguration _config)
		{
			masuk = new MasukModel(_config);
		}
		public IActionResult Index()
		{
			return View();
		}
        public async Task<VMResponse> Masuk(string email, string password, VMPengguna data)
        {
            response = await masuk.Masuk(email, password);

            if (response.statusCode == HttpStatusCode.OK)
            {
                VMPengguna dataPg = (VMPengguna)response.data;
                if (dataPg.Password == password)
                {

                    HttpContext.Session.SetInt32("IdPengguna", (int)dataPg.Id);
                    if(dataPg.IdPetugas != null)
                    {
                        HttpContext.Session.SetInt32("IdPetugas", (int)dataPg.IdPetugas);
                    }
                    else
                    {
                        HttpContext.Session.SetInt32("IdAnggota", (int)dataPg.IdAnggota);
                    }
                    HttpContext.Session.SetString("infoMsg", "Login berhasil");

                }
                else
                {
                    HttpContext.Session.SetString("errMsg", "Email dan Password tidak sama");
                }
            }
            return response;
        }
    }
}
