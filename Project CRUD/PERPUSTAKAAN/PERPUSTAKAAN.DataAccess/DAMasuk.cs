﻿using PERPUSTAKAAN.DataModel;
using PERPUSTAKAAN.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PERPUSTAKAAN.DataAccess
{
	public class DAMasuk
	{
		private readonly PERPUSTAKAANContext db;
		private VMResponse response = new VMResponse();
		public DAMasuk(PERPUSTAKAANContext _db)
		{
			db = _db;
		}
		public VMResponse Masuk(string email, string password)
		{
			try
			{
				VMPengguna? data = (
				   from p in db.Penggunas
				   where p.IsDeleted == false
				   && p.Email == email && p.Password == password
				   select new VMPengguna
				   {
					   Id = p.Id,
					   IdPetugas = p.IdPetugas,
					   IdAnggota = p.IdAnggota,
					   Email = p.Email,
					   Password = p.Password,

					   IsDeleted = p.IsDeleted,
					   DeletedBy = p.DeletedBy,
					   DeletedDate = p.DeletedDate,

					   CreateBy = p.CreateBy,
					   CreateDate = p.CreateDate,
					   UpdateBy = p.UpdateBy,
					   UpdateDate = p.UpdateDate,
				   }
			   ).FirstOrDefault();

				response.data = data;
				response.message = (data == null) ? "Your email and password does not match" : "Login success";
				response.statusCode = (data == null) ? HttpStatusCode.NoContent : HttpStatusCode.OK;
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
			}
			return response;
		}

	}
}
