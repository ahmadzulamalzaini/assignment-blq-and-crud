﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PERPUSTAKAAN.ViewModel
{
	public class VMPengguna
	{
		public int Id { get; set; }
		public int? IdPetugas { get; set; }
		public int? IdAnggota { get; set; }
		public string? Email { get; set; }
		public string? Password { get; set; }
		public bool IsDeleted { get; set; }
		public int? DeletedBy { get; set; }
		public DateTime? DeletedDate { get; set; }
		public int CreateBy { get; set; }
		public DateTime CreateDate { get; set; }
		public int? UpdateBy { get; set; }
		public DateTime? UpdateDate { get; set; }
	}
}
