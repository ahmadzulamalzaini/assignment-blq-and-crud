﻿using Microsoft.AspNetCore.Mvc;
using PERPUSTAKAAN.DataAccess;
using PERPUSTAKAAN.DataModel;
using PERPUSTAKAAN.ViewModel;

namespace PERPUSTAKAAN.API.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class MasukController : Controller
	{
		private readonly DAMasuk masuk;
		public MasukController(PERPUSTAKAANContext _db)
		{
			masuk = new DAMasuk(_db);
		}
		[HttpGet]
		public VMResponse Masuk(string email,string password) 
		{
			return masuk.Masuk(email, password);
		}
	}
}
