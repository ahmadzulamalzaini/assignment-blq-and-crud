﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PERPUSTAKAAN.Models;
using PERPUSTAKAAN.ViewModel;
using System.Net;

namespace PERPUSTAKAAN.Controllers
{
	public class PetugasController : Controller
	{
		private DaftarModel daftar;
		private MasukModel masuk;
		private VMResponse? response;
		private RakModel rak;
		string JsonData;
		public PetugasController(IConfiguration _config)
		{
			daftar = new DaftarModel(_config);
			masuk = new MasukModel(_config);	
			rak = new RakModel(_config);
		}
		public IActionResult Index()
		{
			return View();
		}
        public async Task<IActionResult> Rak()
        {
			VMResponse rakResponse = await rak.GetAll();
			ViewBag.Rak = (List<VMRak>)rakResponse.data;
            return View();
        }
		public IActionResult Tambah()
		{
			return View();
		}
        public IActionResult KonTambah(string namaRak, string lokasiRak, int createBy)
        {
            ViewBag.Nama = namaRak;
            ViewBag.Lokasi = lokasiRak;
            ViewBag.CreateBy = createBy;
            return View();
        }
        [HttpPost]
		public async Task<VMResponse> Tambah(VMRak data)
		{
			response = await rak.Create(data);
            if (response.statusCode == HttpStatusCode.Created)
            {
                HttpContext.Session.SetString("infoMsg", "Penambahan rak baru berhasil");
            }
            else
            {
                HttpContext.Session.SetString("errMsg", "Penambahan rak gagal");
            }
			return response;
        }
		public async Task<bool> CekRak(string NamaRak)
		{
			bool result = false;
			int x = 0;
			VMResponse rakResponse = await rak.GetAll();
			List<VMRak> dataRak = (List<VMRak>)rakResponse.data;
			foreach(VMRak data in dataRak)
			{
				if((data.NamaRak).ToLower() == NamaRak.ToLower())
				{
					x = x + 1;
				}
			}
			if(x == 0)
			{
				result = true;
			}
			
			return result;
		}
        public async Task<IActionResult> EditRak(int id)
        {
			response = await rak.GetById(id);
			ViewBag.Id = id;	
			ViewBag.Nama = ((VMRak)response.data).NamaRak;
			ViewBag.Lokasi = ((VMRak)response.data).LokasiRak;
            return View();
        }
        public async Task<bool> CekNamaRak(string NamaRak, string nama)
        {
            bool result = false;
            int x = 0;
            VMResponse rakResponse = await rak.GetAll();
            List<VMRak> dataRak = (List<VMRak>)rakResponse.data;
            foreach (VMRak data in dataRak)
            {
                if ((data.NamaRak).ToLower() == NamaRak.ToLower())
                {
                    if(nama.ToLower() != data.NamaRak.ToLower())
                    {
                        x = x + 1;
                    }
                }
            }
            if (x == 0)
            {
                result = true;
            }

            return result;
        }
        public IActionResult KonEditRak(string namaRak, string lokasiRak, int updateBy, int id)
		{
			ViewBag.Nama = namaRak;
            ViewBag.Id = id;
            ViewBag.Lokasi = lokasiRak;
			ViewBag.UpdateBy = updateBy;
			return View();
        }
        [HttpPost]
        public async Task<VMResponse> EditRak(VMRak data)
        {
            response = await rak.Update(data);
            if (response.statusCode == HttpStatusCode.OK)
            {
                HttpContext.Session.SetString("infoMsg", "Edit data rak berhasil");
            }
            else
            {
                HttpContext.Session.SetString("errMsg", "Edit data rak gagal");
            }
            return response;
        }
        public async Task<IActionResult> Hapus(int id)
        {
            response = await rak.GetById(id);
            ViewBag.Id = id;
            ViewBag.Nama = ((VMRak)response.data).NamaRak;
            return View();
        }

        [HttpPost]
        public async Task<VMResponse> Hapus(int id, int idPengguna)
        {
            response = await rak.Delete(id, idPengguna);
            if (response.statusCode == HttpStatusCode.OK)
            {
                HttpContext.Session.SetString("infoMsg", "Hapus rak berhasil");
            }
            else
            {
                HttpContext.Session.SetString("errMsg", "Hapus rak gagal");
            }
            return response;
        }
    }
}
