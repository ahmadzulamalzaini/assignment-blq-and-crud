create database PERPUSTAKAAN


CREATE TABLE Anggota (
  [id] [int] IDENTITY(1,1) NOT NULL,
  kode_anggota varchar(10) NOT NULL,
  nama_anggota varchar(100) NOT NULL,
  jk_anggota char(1) NOT NULL,
  jurusan_anggota varchar(50) NOT NULL,
  no_telp_anggota varchar(13) NOT NULL,
  alamat_anggota varchar(100) NOT NULL,

	[is_deleted] [bit] default (0) NOT NULL,
	[deleted_by] [int] NULL,
	[deleted_date] [datetime] NULL,
	[create_by] [int] NOT NULL,
	[create_date] [datetime] default (getdate()) NOT NULL,
	[update_by] [int] NULL,
	[update_date] [datetime] NULL,
 CONSTRAINT [PK_Anggota] PRIMARY KEY(id)
)

CREATE TABLE Buku (
   id int IDENTITY(1,1) NOT NULL,
   id_rak int,
   kode_buku char(50) NOT NULL,
   judul_buku varchar(50) NOT NULL,
   penulis_buku varchar(50) NOT NULL,
   penerbit_buku varchar(50) NOT NULL,
   tahun_penerbit char(4) NOT NULL,
   nama_gambar varchar(100),
   file_gambar  varbinary(max),
   stok int NOT NULL,

   [is_deleted] [bit] default (0) NOT NULL,
	[deleted_by] [int] NULL,
	[deleted_date] [datetime] NULL,
	[create_by] [int] NOT NULL,
	[create_date] [datetime] default (getdate()) NOT NULL,
	[update_by] [int] NULL,
	[update_date] [datetime] NULL
	CONSTRAINT [PK_Buku] PRIMARY KEY(id)
)

CREATE TABLE Peminjaman (
  id int IDENTITY(1,1) NOT NULL,
  tanggal_pinjam datetime NOT NULL,
  tanggal_kembali datetime NOT NULL,
  id_buku int NOT NULL,
  id_anggota int NOT NULL,
  id_petugas int NOT NULL,

  [is_deleted] [bit] default (0) NOT NULL,
	[deleted_by] [int] NULL,
	[deleted_date] [datetime] NULL,
	[create_by] [int] NOT NULL,
	[create_date] [datetime] default (getdate()) NOT NULL,
	[update_by] [int] NULL,
	[update_date] [datetime] NULL
	CONSTRAINT [PK_Peminjaman] PRIMARY KEY(id)
)

CREATE TABLE Pengembalian (
  id int IDENTITY(1,1) NOT NULL,
  tanggal_pengembalian datetime NOT NULL,
  denda int NOT NULL,
  id_buku int NOT NULL,
  id_anggota int NOT NULL,
  id_petugas int NOT NULL,

    [is_deleted] [bit] default (0) NOT NULL,
	[deleted_by] [int] NULL,
	[deleted_date] [datetime] NULL,
	[create_by] [int] NOT NULL,
	[create_date] [datetime] default (getdate()) NOT NULL,
	[update_by] [int] NULL,
	[update_date] [datetime] NULL
	CONSTRAINT [PK_Pengembalian] PRIMARY KEY(id)
)

CREATE TABLE Petugas (
  id int IDENTITY(1,1) NOT NULL,
  nama_petugas varchar(50) NOT NULL,
  jabatan_petugas varchar(50) NOT NULL,
  no_telp_petugas varchar(13) NOT NULL,
  alamat_petugas varchar(100) NOT NULL,

    [is_deleted] [bit] default (0) NOT NULL,
	[deleted_by] [int] NULL,
	[deleted_date] [datetime] NULL,
	[create_by] [int] NOT NULL,
	[create_date] [datetime] default (getdate()) NOT NULL,
	[update_by] [int] NULL,
	[update_date] [datetime] NULL
	CONSTRAINT [PK_Petugas] PRIMARY KEY(id)
)

CREATE TABLE Rak (
  id int IDENTITY(1,1) NOT NULL,
  nama_rak varchar(50) NOT NULL,
  lokasi_rak varchar(50) NOT NULL,

    [is_deleted] [bit] default (0) NOT NULL,
	[deleted_by] [int] NULL,
	[deleted_date] [datetime] NULL,
	[create_by] [int] NOT NULL,
	[create_date] [datetime] default (getdate()) NOT NULL,
	[update_by] [int] NULL,
	[update_date] [datetime] NULL
	CONSTRAINT [PK_Rak] PRIMARY KEY(id)
)

create table Pengguna(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_petugas] [int] NULL,
	[id_anggota] [int] NULL,
	[email] [varchar](100) NULL,
	[password] [varchar](255) NULL,

    [is_deleted] [bit] default (0) NOT NULL,
	[deleted_by] [int] NULL,
	[deleted_date] [datetime] NULL,
	[create_by] [int] NOT NULL,
	[create_date] [datetime] default (getdate()) NOT NULL,
	[update_by] [int] NULL,
	[update_date] [datetime] NULL
 CONSTRAINT [Pk_Pengguna] PRIMARY KEY(id)
)