﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PERPUSTAKAAN.DataModel
{
    [Table("Peminjaman")]
    public partial class Peminjaman
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("tanggal_pinjam", TypeName = "datetime")]
        public DateTime TanggalPinjam { get; set; }
        [Column("tanggal_kembali", TypeName = "datetime")]
        public DateTime TanggalKembali { get; set; }
        [Column("id_buku")]
        public int IdBuku { get; set; }
        [Column("id_anggota")]
        public int IdAnggota { get; set; }
        [Column("id_petugas")]
        public int IdPetugas { get; set; }
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }
        [Column("deleted_by")]
        public int? DeletedBy { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Column("create_by")]
        public int CreateBy { get; set; }
        [Column("create_date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("update_by")]
        public int? UpdateBy { get; set; }
        [Column("update_date", TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
    }
}
