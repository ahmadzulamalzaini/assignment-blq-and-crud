﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PERPUSTAKAAN.DataModel
{
    [Table("Buku")]
    public partial class Buku
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("id_rak")]
        public int? IdRak { get; set; }
        [Column("kode_buku")]
        [StringLength(50)]
        [Unicode(false)]
        public string KodeBuku { get; set; } = null!;
        [Column("judul_buku")]
        [StringLength(50)]
        [Unicode(false)]
        public string JudulBuku { get; set; } = null!;
        [Column("penulis_buku")]
        [StringLength(50)]
        [Unicode(false)]
        public string PenulisBuku { get; set; } = null!;
        [Column("penerbit_buku")]
        [StringLength(50)]
        [Unicode(false)]
        public string PenerbitBuku { get; set; } = null!;
        [Column("tahun_penerbit")]
        [StringLength(4)]
        [Unicode(false)]
        public string TahunPenerbit { get; set; } = null!;
        [Column("nama_gambar")]
        [StringLength(100)]
        [Unicode(false)]
        public string? NamaGambar { get; set; }
        [Column("file_gambar")]
        public byte[]? FileGambar { get; set; }
        [Column("stok")]
        public int Stok { get; set; }
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }
        [Column("deleted_by")]
        public int? DeletedBy { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Column("create_by")]
        public int CreateBy { get; set; }
        [Column("create_date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("update_by")]
        public int? UpdateBy { get; set; }
        [Column("update_date", TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
    }
}
