﻿using PERPUSTAKAAN.DataModel;
using PERPUSTAKAAN.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PERPUSTAKAAN.DataAccess
{
    public class DARak
    {
        private readonly PERPUSTAKAANContext db;
        public DARak(PERPUSTAKAANContext _db)
        {
            db = _db;
        }
        private VMResponse response = new VMResponse();
        public VMResponse GetAll()
        {
            try
            {
                List<VMRak> data = (
                    from r in db.Raks
                    where r.IsDeleted == false
                    select new VMRak
                    {
                        Id = r.Id,
                        NamaRak = r.NamaRak,
                        LokasiRak = r.LokasiRak,

                        IsDeleted = r.IsDeleted,
                        DeletedDate = r.DeletedDate,
                        CreateBy   = r.CreateBy,
                        CreateDate = r.CreateDate,
                        UpdateBy = r.UpdateBy,
                        UpdateDate = r.UpdateDate,
                    }
                ).ToList();

                response.data = data;
                response.message = (data.Count < 1) ? "Data rak tidak ada" : "Data rak ditemukan";
                response.statusCode = (data.Count < 1) ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }
        public VMRak? FindById(int id)
        {
            return (
                from r in db.Raks
                where r.IsDeleted == false && r.Id == id
                select new VMRak
                {
                    Id = r.Id,
                    NamaRak = r.NamaRak,
                    LokasiRak = r.LokasiRak,

                    IsDeleted = r.IsDeleted,
                    DeletedDate = r.DeletedDate,
                    CreateBy = r.CreateBy,
                    CreateDate = r.CreateDate,
                    UpdateBy = r.UpdateBy,
                    UpdateDate = r.UpdateDate,
                }
            ).FirstOrDefault();
        }
        public VMResponse GetsById(int id)
        {
            try
            {
                VMRak? data = FindById(id);
                response.data = data;
                response.message = (data == null)
                    ? $"Rak dengan id = {id} tidak ditemukan"
                    : $"Rak dengan id = {id} berhasil ditemukan";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }
        public VMResponse Create(VMRak dataInput)
        {
            try
            {
                Rak data = new Rak();
                data.NamaRak = dataInput.NamaRak;
                data.LokasiRak = dataInput.LokasiRak;

                data.IsDeleted = false;
                data.CreateBy = dataInput.CreateBy;
                data.CreateDate = DateTime.Now;

                //prosess create new data in table from data model
                db.Add(data);
                db.SaveChanges();

                //update api response
                response.data = data;
                response.message = "Penambahan Rak Berhasil";
                response.statusCode = HttpStatusCode.Created;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }
        public VMResponse Update(VMRak dataInput)
        {
            try
            {
                VMRak? dataExist = FindById(dataInput.Id);

                if (dataExist != null)
                {
                    Rak data = new Rak();
                    data.Id = dataExist.Id;
                    data.NamaRak = dataInput.NamaRak;
                    data.LokasiRak = dataInput.LokasiRak;

                    data.IsDeleted = false;
                    data.CreateBy = dataExist.Id;
                    data.CreateDate = dataExist.CreateDate;

                    data.UpdateBy = dataInput.UpdateBy;
                    data.UpdateDate = DateTime.Now;

                    db.Update(data);
                    db.SaveChanges();

                    //update api response
                    response.data = data;
                    response.message = "Berhasil menambahkan rak baru";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    throw new Exception(response.message);
                }


            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }

            return response;
        }
        public VMResponse Delete(int id, int userId)
        {
            try
            {
                VMRak? dataExist = FindById(id);
                if (dataExist != null)
                {
                    Rak data = new Rak();
                    data.Id = dataExist.Id;
                    data.NamaRak = dataExist.NamaRak;
                    data.LokasiRak = dataExist.LokasiRak;

                    data.IsDeleted = true;
                    data.DeletedBy = userId;
                    data.DeletedDate = DateTime.Now;
                    data.CreateBy = dataExist.Id;
                    data.CreateDate = dataExist.CreateDate;

                    data.UpdateBy = dataExist.UpdateBy;
                    data.UpdateDate = dataExist.UpdateDate;


                    db.Update(data);
                    db.SaveChanges();
                    response.data = data;

                    response.message = ($"Delete berhasil");
                    response.statusCode = HttpStatusCode.OK;
                }
            }
            catch
            {
                response.message = $"Delete gagal";
            }

            return response;
        }

    }
}
