﻿using Newtonsoft.Json;
using PERPUSTAKAAN.ViewModel;
using System.Net;
using System.Text;

namespace PERPUSTAKAAN.Models
{
    public class DaftarModel
    {
        private string? jsonData;
        private HttpContent? content;
        private VMResponse? apiResponse;
        private readonly string? apiUrl;
        private readonly HttpClient httpClient;

        public DaftarModel(IConfiguration _config)
        {
            apiUrl = _config["ApiUrl"];
            httpClient = new HttpClient();
        }
        public async Task<VMResponse> GetAllPetugas()
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await
                    httpClient.GetStringAsync(apiUrl + "api/Daftar/GetAllPetugas"));
                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK || apiResponse.statusCode ==
                        HttpStatusCode.NoContent)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<
                            List<VMPetugas>>(apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("tidak bisa diakses");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message = $" {ex.Message}";
                apiResponse.data = new List<VMPetugas>();

            }
            return apiResponse;
        }
        public async Task<VMResponse> GetAllAnggota()
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await
                    httpClient.GetStringAsync(apiUrl + "api/Daftar/GetAllAnggota"));
                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK || apiResponse.statusCode ==
                        HttpStatusCode.NoContent)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<
                            List<VMAnggota>>(apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("tidak bisa diakses");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message = $" {ex.Message}";
                apiResponse.data = new List<VMAnggota>();

            }
            return apiResponse;
        }
		public async Task<VMResponse> GetAllPengguna()
		{
			try
			{
				apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await
					httpClient.GetStringAsync(apiUrl + "api/Daftar/GetAllPengguna"));
				if (apiResponse != null)
				{
					if (apiResponse.statusCode == HttpStatusCode.OK || apiResponse.statusCode ==
						HttpStatusCode.NoContent)
					{
						apiResponse.data = JsonConvert.DeserializeObject<
							List<VMPengguna>>(apiResponse.data.ToString());
					}
					else
					{
						throw new Exception(apiResponse.message);
					}
				}
				else
				{
					throw new Exception("tidak bisa diakses");
				}
			}
			catch (Exception ex)
			{
				apiResponse.message = $" {ex.Message}";
				apiResponse.data = new List<VMPengguna>();

			}
			return apiResponse;
		}
		public async Task<VMResponse> GetPetugasById(int id)
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await
                    httpClient.GetStringAsync(apiUrl + $"api/Daftar/GetPetugasById/?id={id}"));
                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK || apiResponse.statusCode ==
                        HttpStatusCode.NoContent)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<
                            VMPetugas>(apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("tidak bisa diakses");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message = $" {ex.Message}";
                apiResponse.data = new List<VMPetugas>();

            }
            return apiResponse;
        }
        public async Task<VMResponse> GetAnggotaById(int id)
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await
                    httpClient.GetStringAsync(apiUrl + $"api/Daftar/GetAnggotaById/?id={id}"));
                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK || apiResponse.statusCode ==
                        HttpStatusCode.NoContent)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<
                            VMAnggota>(apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("tidak bisa diakses");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message = $" {ex.Message}";
                apiResponse.data = new List<VMAnggota>();

            }
            return apiResponse;
        }
		public async Task<VMResponse> GetPenggunaById(int id)
		{
			try
			{
				apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await
					httpClient.GetStringAsync(apiUrl + $"api/Daftar/GetPenggunaById/?id={id}"));
				if (apiResponse != null)
				{
					if (apiResponse.statusCode == HttpStatusCode.OK || apiResponse.statusCode ==
						HttpStatusCode.NoContent)
					{
						apiResponse.data = JsonConvert.DeserializeObject<
							VMPengguna>(apiResponse.data.ToString());
					}
					else
					{
						throw new Exception(apiResponse.message);
					}
				}
				else
				{
					throw new Exception("tidak bisa diakses");
				}
			}
			catch (Exception ex)
			{
				apiResponse.message = $" {ex.Message}";
				apiResponse.data = new List<VMPengguna>();

			}
			return apiResponse;
		}
		public async Task<VMResponse> DaftarPetugas(VMPetugas data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>
                    (await (await httpClient.PostAsync(apiUrl + "api/Daftar/DaftarPetugas", content)).Content.ReadAsStringAsync());

                if (apiResponse == null)
                {
                    throw new Exception("API cannot be reached");
                }

            }
            catch (Exception ex)
            {
                apiResponse.message += $"  {ex.Message}";
            }
            return apiResponse;
        }
        public async Task<VMResponse> DaftarAnggota(VMAnggota data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await httpClient.PostAsync(apiUrl + "api/Daftar/DaftarAnggota", content)).Content.ReadAsStringAsync());
                if (apiResponse == null)
                {
                    throw new Exception(" API Cannot Be Reached!");
                }
                else
                {
                    throw (new Exception (apiResponse.message));
                }
            }
            catch (Exception ex)
            {
                apiResponse.message += $"{ex.Message}";
            }
            return apiResponse;
        }
        public async Task<VMResponse> Daftar(VMPengguna data)
		{
			try
			{
				jsonData = JsonConvert.SerializeObject(data);
				content = new StringContent(jsonData, Encoding.UTF8, "application/json");
				apiResponse = JsonConvert.DeserializeObject<VMResponse?>
					(await (await httpClient.PostAsync(apiUrl + "api/Daftar/Daftar", content)).Content.ReadAsStringAsync());

				if (apiResponse == null)
				{
					throw new Exception("API cannot be reached");
				}

			}
			catch (Exception ex)
			{
				apiResponse.message += $"  {ex.Message}";
			}
			return apiResponse;
		}
		public async Task<VMResponse> UpdatePetugas(VMPetugas data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>
                    (await (await httpClient.PutAsync(apiUrl + "api/Daftar/UpdatePetugas", content)).Content.ReadAsStringAsync());

                if (apiResponse == null)
                {
                    throw new Exception("API cannot be reached");
                }

            }
            catch (Exception ex)
            {
                apiResponse.message += $"  {ex.Message}";
            }
            return apiResponse;
        }
        public async Task<VMResponse> UpdateAnggota(VMAnggota data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>
                    (await (await httpClient.PutAsync(apiUrl + "api/Daftar/UpdateAnggota", content)).Content.ReadAsStringAsync());

                if (apiResponse == null)
                {
                    throw new Exception("API cannot be reached");
                }

            }
            catch (Exception ex)
            {
                apiResponse.message += $"  {ex.Message}";
            }
            return apiResponse;
        }
        public async Task<VMResponse> Update(VMPengguna data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>
                    (await (await httpClient.PutAsync(apiUrl + "api/Daftar/Update", content)).Content.ReadAsStringAsync());

                if (apiResponse == null)
                {
                    throw new Exception("API cannot be reached");
                }

            }
            catch (Exception ex)
            {
                apiResponse.message += $"  {ex.Message}";
            }
            return apiResponse;
        }
	}
}
