﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_BLQ
{
    public class Menu
    {
        static void PilihanSoal()
        {
            Console.WriteLine("SOAL ASSIGNMENT BLQ");
            Console.WriteLine("=".PadRight(20, '='));
            Console.WriteLine("1.  SOAL 01     12.  SOAL 12");
            Console.WriteLine("2.  SOAL 02     13.  SOAL 13 ");
            Console.WriteLine("3.  SOAL 03     14.  SOAL 14 ");
            Console.WriteLine("4.  SOAL 04     15.  SOAL 15 ");
            Console.WriteLine("5.  SOAL 05     16.  SOAL 16 ");
            Console.WriteLine("6.  SOAL 06     17.  SOAL 17 ");
            Console.WriteLine("7.  SOAL 07     18.  SOAL 18 ");
            Console.WriteLine("8.  SOAL 08     19.  SOAL 19 ");
            Console.WriteLine("9.  SOAL 09     20.  SOAL 20 ");
            Console.WriteLine("10. SOAL 10     21.  SOAL 21 ");
            Console.WriteLine("11. SOAL 11     22.  SOAL 22 ");

            Console.WriteLine("=".PadRight(20, '='));
            Console.Write("Masukkan Pilihan (Enter untuk keluar): ".ToUpper());
        }
        static public void MenuSoal()
        {
            PilihanSoal();
            Console.WriteLine("\n");
            Console.Write("Anda ingin melihat soal nomor berapa: ");
            int nomor = int.Parse(Console.ReadLine());

            Soal soal = new Soal();
            if ( nomor == 1 )
            {
                Console.Clear();
                soal.Soal1();
                Perulangan();
            }
            else if( nomor == 2 )
            {
                Console.Clear();
                soal.Soal2();
                Perulangan();
            }
            else if (nomor == 3)
            {
                Console.Clear();
                soal.Soal3();
                Perulangan();
            }
            else if (nomor == 4)
            {
                Console.Clear();
                soal.Soal4();
                Perulangan();
            }
            else if (nomor == 5)
            {
                Console.Clear();
                soal.Soal5();
                Perulangan();
            }
            else if (nomor == 6)
            {
                Console.Clear();
                soal.Soal6();
                Perulangan();
            }
            else if (nomor == 7)
            {
                Console.Clear();
                soal.Soal7();
                Perulangan();
            }
            else if (nomor == 8)
            {
                Console.Clear();
                soal.Soal8();
                Perulangan();
            }
            else if (nomor == 9)
            {
                Console.Clear();
                soal.Soal9();
                Perulangan();
            }
            else if (nomor == 10)
            {
                Console.Clear();
                soal.Soal10();
                Perulangan();
            }
            else if (nomor == 11)
            {
                Console.Clear();
                soal.Soal11();
                Perulangan();
            }
            else if (nomor == 12)
            {
                Console.Clear();
                soal.Soal12();
                Perulangan();
            }
            else if (nomor == 13)
            {
                Console.Clear();
                soal.Soal13();
                Perulangan();
            }
            else if (nomor == 14)
            {
                Console.Clear();
                soal.Soal14();
                Perulangan();
            }
            else if (nomor == 15)
            {
                Console.Clear();
                soal.Soal15();
                Perulangan();
            }
            else if (nomor == 16)
            {
                Console.Clear();
                soal.Soal16();
                Perulangan();
            }
            else if (nomor == 17)
            {
                Console.Clear();
                soal.Soal17();
                Perulangan();
            }
            else if (nomor == 18)
            {
                Console.Clear();
                soal.Soal18();
                Perulangan();
            }
            else if (nomor == 19)
            {
                Console.Clear();
                soal.Soal19();
                Perulangan();
            }
            else if (nomor == 20)
            {
                Console.Clear();
                soal.Soal20();
                Perulangan();
            }
            else if (nomor == 21)
            {
                Console.Clear();
                soal.Soal21();
                Perulangan();
            }
            else if (nomor == 22)
            {
                Console.Clear();
                soal.Soal22();
                Perulangan();
            }
            else
            {
                Perulangan();
            }

        }
        static void Perulangan()
        {
            Console.WriteLine("\n");
            Console.WriteLine("=".PadRight(52, '='));
            Console.WriteLine("Apakah anda ingin menjalankan soal selanjutnya? (y/n)");
            Console.WriteLine("=".PadRight(52, '='));

            switch (Console.ReadKey(true).Key)
            {
                case ConsoleKey.Y:
                    Console.Clear();
                    MenuSoal();
                    break;
                case ConsoleKey.N:
                    Console.Clear();
                    Console.WriteLine("=".PadRight(35, '='));
                    Console.WriteLine("Terimakasih telah mencoba");
                    Console.WriteLine("=".PadRight(35, '='));
                    break;
                default:
                    Console.WriteLine("=".PadRight(30, '='));
                    Console.WriteLine("Input perintah yang sesuai!!!");
                    Console.WriteLine("=".PadRight(30, '='));
                    Perulangan();
                    break;
            }
        }
    }
}
