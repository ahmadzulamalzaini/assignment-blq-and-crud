﻿using Microsoft.AspNetCore.Mvc;
using PERPUSTAKAAN.Models;
using PERPUSTAKAAN.ViewModel;

namespace PERPUSTAKAAN.Controllers
{
    public class AnggotaController : Controller
    {
        private DaftarModel daftar;
        private MasukModel masuk;
        private VMResponse? response;
        private RakModel rak;
        string JsonData;
        public AnggotaController(IConfiguration _config)
        {
            daftar = new DaftarModel(_config);
            masuk = new MasukModel(_config);
            rak = new RakModel(_config);
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> Rak()
        {
            VMResponse rakResponse = await rak.GetAll();
            ViewBag.Rak = (List<VMRak>)rakResponse.data;
            return View();
        }
        public IActionResult Konfirmasi()
        {
            return View();
        }
    }
}
