﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PERPUSTAKAAN.DataModel
{
    public partial class Petuga
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("nama_petugas")]
        [StringLength(50)]
        [Unicode(false)]
        public string NamaPetugas { get; set; } = null!;
        [Column("jabatan_petugas")]
        [StringLength(50)]
        [Unicode(false)]
        public string JabatanPetugas { get; set; } = null!;
        [Column("no_telp_petugas")]
        [StringLength(13)]
        [Unicode(false)]
        public string NoTelpPetugas { get; set; } = null!;
        [Column("alamat_petugas")]
        [StringLength(100)]
        [Unicode(false)]
        public string AlamatPetugas { get; set; } = null!;
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }
        [Column("deleted_by")]
        public int? DeletedBy { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Column("create_by")]
        public int CreateBy { get; set; }
        [Column("create_date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("update_by")]
        public int? UpdateBy { get; set; }
        [Column("update_date", TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
    }
}
