﻿using PERPUSTAKAAN.DataModel;
using PERPUSTAKAAN.ViewModel;
using System.Net;

namespace PERPUSTAKAAN.DataAccess
{
    public class DADaftar
    {
        private readonly PERPUSTAKAANContext db;
        public DADaftar(PERPUSTAKAANContext _db)
        {
            db = _db;
        }
        private VMResponse response = new VMResponse();
        public VMResponse GetAllPetugas()
        {
            try
            {
                List<VMPetugas> data = (
                    from p in db.Petugas
                    where p.IsDeleted == false
                    select new VMPetugas
                    {
                        Id = p.Id,
                        NamaPetugas = p.NamaPetugas,
                        JabatanPetugas = p.JabatanPetugas,
                        NoTelpPetugas = p.NoTelpPetugas,
                        AlamatPetugas = p.AlamatPetugas,

                        IsDeleted = p.IsDeleted,
                        DeletedBy = p.DeletedBy,
                        DeletedDate = p.DeletedDate,

                        CreateBy = p.CreateBy,
                        CreateDate = p.CreateDate,
                        UpdateBy = p.UpdateBy,
                        UpdateDate = p.UpdateDate,
                    }
                ).ToList();

                response.data = data;
                response.message = (data.Count < 1) ? "Data petugas tidak ada" : "Data petugas ditemukan";
                response.statusCode = (data.Count < 1) ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }
        public VMPetugas? FindPetugasById(int id)
        {
            return (
                from p in db.Petugas
                where p.IsDeleted == false && p.Id == id
                select new VMPetugas
                {
                    Id = p.Id,
                    NamaPetugas = p.NamaPetugas,
                    JabatanPetugas = p.JabatanPetugas,
                    NoTelpPetugas = p.NoTelpPetugas,
                    AlamatPetugas = p.AlamatPetugas,

                    IsDeleted = p.IsDeleted,
                    DeletedBy = p.DeletedBy,
                    DeletedDate = p.DeletedDate,

                    CreateBy = p.CreateBy,
                    CreateDate = p.CreateDate,
                    UpdateBy = p.UpdateBy,
                    UpdateDate = p.UpdateDate,
                }
            ).FirstOrDefault();
        }
        public VMResponse GetPetugasById(int id)
        {
            try
            {
                VMPetugas? data = FindPetugasById(id);
                response.data = data;
                response.message = (data == null)
                    ? $"Petugas dengan id = {id} tidak ditemukan"
                    : $"Petugas dengan id = {id} berhasil ditemukan";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }
        public VMResponse GetAllAnggota()
        {
            try
            {
                List<VMAnggota> data = (
                    from a in db.Anggota
                    where a.IsDeleted == false
                    select new VMAnggota
                    {
                        Id = a.Id,
                        KodeAnggota = a.KodeAnggota,
                        NamaAnggota = a.NamaAnggota,
                        JkAnggota = a.JkAnggota,
                        JurusanAnggota = a.JurusanAnggota,
                        NoTelpAnggota = a.NoTelpAnggota,
                        AlamatAnggota = a.AlamatAnggota,

                        IsDeleted = a.IsDeleted,
                        DeletedBy = a.DeletedBy,
                        DeletedDate = a.DeletedDate,

                        CreateBy = a.CreateBy,
                        CreateDate = a.CreateDate,
                        UpdateBy = a.UpdateBy,
                        UpdateDate = a.UpdateDate,
                    }
                ).ToList();

                response.data = data;
                response.message = (data.Count < 1) ? "Data anggota tidak ada" : "Data anggota ditemukan";
                response.statusCode = (data.Count < 1) ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }
        public VMAnggota? FindAnggotaById(int id)
        {
            return (
                from a in db.Anggota
                where a.IsDeleted == false && a.Id == id
                select new VMAnggota
                {
                    Id = a.Id,
                    KodeAnggota = a.KodeAnggota,
                    NamaAnggota = a.NamaAnggota,
                    JkAnggota = a.JkAnggota,
                    JurusanAnggota = a.JurusanAnggota,
                    NoTelpAnggota = a.NoTelpAnggota,
                    AlamatAnggota = a.AlamatAnggota,

                    IsDeleted = a.IsDeleted,
                    DeletedBy = a.DeletedBy,
                    DeletedDate = a.DeletedDate,

                    CreateBy = a.CreateBy,
                    CreateDate = a.CreateDate,
                    UpdateBy = a.UpdateBy,
                    UpdateDate = a.UpdateDate,
                }
            ).FirstOrDefault();
        }
        public VMResponse GetAnggotaById(int id)
        {
            try
            {
                VMAnggota? data = FindAnggotaById(id);
                response.data = data;
                response.message = (data == null)
                    ? $"Anggota dengan id = {id} tidak ditemukan"
                    : $"Anggota dengan id = {id} berhasil ditemukan";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }
        public VMResponse DaftarPetugas(VMPetugas dataInput)
        {
            try
            {
                Petuga data = new Petuga();
                data.NamaPetugas = dataInput.NamaPetugas;
                data.JabatanPetugas = dataInput.JabatanPetugas;
                data.NoTelpPetugas = dataInput.NoTelpPetugas;
                data.AlamatPetugas = dataInput.AlamatPetugas;

                data.IsDeleted = false;
                data.CreateDate = DateTime.Now;


                //prosess create new data in table from data model
                db.Add(data);
                db.SaveChanges();

                //update api response
                response.data = data;
                response.message = "Proses pendaftaran berhasil";
                response.statusCode = HttpStatusCode.Created;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }
        public VMResponse DaftarAnggota(VMAnggota dataInput)
        {
            try
            {
                Anggotum data = new  Anggotum();
                data.KodeAnggota = "AGT-";
                data.NamaAnggota = dataInput.NamaAnggota;
                data.JkAnggota = dataInput.JkAnggota;
                data.JurusanAnggota = dataInput.JurusanAnggota;
                data.NoTelpAnggota = dataInput.NoTelpAnggota;
                data.AlamatAnggota = dataInput.AlamatAnggota;

                data.IsDeleted = false;
                data.CreateDate = DateTime.Now;


                //prosess create new data in table from data model
                db.Add(data);
                db.SaveChanges();

                //update api response
                response.data = data;
                response.message = "Proses pendaftaran berhasil";
                response.statusCode = HttpStatusCode.Created;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }
        public VMResponse UpdatePetugas(VMPetugas dataInput)
        {
            try
            {
                VMPetugas? dataExist = FindPetugasById(dataInput.Id);

                if (dataExist != null)
                {
                    Petuga data = new Petuga();
                    data.Id = dataExist.Id;
                    data.NamaPetugas = dataExist.NamaPetugas;
                    data.JabatanPetugas = dataExist.JabatanPetugas;
                    data.NoTelpPetugas = dataExist.NoTelpPetugas;
                    data.AlamatPetugas = dataExist.AlamatPetugas;

                    data.IsDeleted = false;
                    data.CreateBy = dataExist.Id;
                    data.CreateDate = DateTime.Now;

                    db.Update(data);
                    db.SaveChanges();

                    //update api response
                    response.data = data;
                    response.message = "Berhasil menambahkan create by petugas";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    throw new Exception(response.message);
                }


            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }

            return response;
        }
        public VMResponse UpdateAnggota(VMAnggota dataInput)
        {
            try
            {
                VMAnggota? dataExist = FindAnggotaById(dataInput.Id);

                if (dataExist != null)
                {
                    Anggotum data = new Anggotum();
                    data.Id = dataExist.Id;
                    data.KodeAnggota = dataExist.KodeAnggota + dataInput.KodeAnggota ;
                    data.NamaAnggota = dataExist.NamaAnggota;
                    data.JkAnggota = dataExist.JkAnggota;
                    data.JurusanAnggota = dataExist.JurusanAnggota;
                    data.NoTelpAnggota = dataExist.NoTelpAnggota;
                    data.AlamatAnggota = dataExist.AlamatAnggota;

                    data.IsDeleted = false;
                    data.CreateBy = dataExist.Id;
                    data.CreateDate = DateTime.Now;

                    db.Update(data);
                    db.SaveChanges();

                    //update api response
                    response.data = data;
                    response.message = "Berhasil menambahkan create by anggota";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    throw new Exception(response.message);
                }


            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }

            return response;
        }
		public VMResponse Daftar(VMPengguna dataInput)
		{
			try
			{
				Pengguna data = new Pengguna();
                data.IdPetugas = dataInput.IdPetugas;
                data.IdAnggota = dataInput.IdAnggota;
                data.Email = dataInput.Email;
                data.Password = dataInput.Password;

				data.IsDeleted = false;
				data.CreateDate = DateTime.Now;


				//prosess create new data in table from data model
				db.Add(data);
				db.SaveChanges();

				//update api response
				response.data = data;
				response.message = "Proses pendaftaran berhasil";
				response.statusCode = HttpStatusCode.Created;
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
			}
			return response;
		}
		public VMResponse GetAllPengguna()
		{
			try
			{
				List<VMPengguna> data = (
					from p in db.Penggunas
					where p.IsDeleted == false
					select new VMPengguna
					{
						Id = p.Id,
						IdPetugas = p.IdPetugas,
                        IdAnggota = p.IdAnggota,
                        Email = p.Email,
                        Password = p.Password,

						IsDeleted = p.IsDeleted,
						DeletedBy = p.DeletedBy,
						DeletedDate = p.DeletedDate,

						CreateBy = p.CreateBy,
						CreateDate = p.CreateDate,
						UpdateBy = p.UpdateBy,
						UpdateDate = p.UpdateDate,
					}
				).ToList();

				response.data = data;
				response.message = (data.Count < 1) ? "Data pengguna tidak ada" : "Data pengguna ditemukan";
				response.statusCode = (data.Count < 1) ? HttpStatusCode.NoContent : HttpStatusCode.OK;
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
			}
			return response;
		}
		public VMPengguna? FindPenggunaById(int id)
		{
			return (
				from p in db.Penggunas
				where p.IsDeleted == false && p.Id == id
				select new VMPengguna
				{
					Id = p.Id,
					IdPetugas = p.IdPetugas,
					IdAnggota = p.IdAnggota,
					Email = p.Email,
					Password = p.Password,

					IsDeleted = p.IsDeleted,
					DeletedBy = p.DeletedBy,
					DeletedDate = p.DeletedDate,

					CreateBy = p.CreateBy,
					CreateDate = p.CreateDate,
					UpdateBy = p.UpdateBy,
					UpdateDate = p.UpdateDate,
				}
			).FirstOrDefault();
		}
		public VMResponse GetPenggunaById(int id)
		{
			try
			{
				VMPengguna? data = FindPenggunaById(id);
				response.data = data;
				response.message = (data == null)
					? $"Pengguna dengan id = {id} tidak ditemukan"
					: $"Pengguna dengan id = {id} berhasil ditemukan";
				response.statusCode = (data == null)
					? HttpStatusCode.NoContent : HttpStatusCode.OK;
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
			}
			return response;
		}
		public VMResponse Update(VMPengguna dataInput)
		{
			try
			{
				VMPengguna? dataExist = FindPenggunaById(dataInput.Id);

				if (dataExist != null)
				{
					Pengguna data = new Pengguna();
					data.Id = dataExist.Id;
					data.IdPetugas = dataExist.IdPetugas;
					data.IdAnggota = dataExist.IdAnggota;
					data.Email = dataExist.Email;
					data.Password = dataExist.Password;

					data.IsDeleted = false;
					data.CreateBy = dataExist.Id;
					data.CreateDate = DateTime.Now;

					db.Update(data);
					db.SaveChanges();

					//update api response
					response.data = data;
					response.message = "Berhasil menambahkan create by pengguna";
					response.statusCode = HttpStatusCode.OK;
				}
				else
				{
					throw new Exception(response.message);
				}


			}
			catch (Exception ex)
			{
				response.message = ex.Message;
			}

			return response;
		}
	}
}