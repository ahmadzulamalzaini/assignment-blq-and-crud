﻿using Microsoft.AspNetCore.Mvc;
using PERPUSTAKAAN.DataAccess;
using PERPUSTAKAAN.DataModel;
using PERPUSTAKAAN.ViewModel;

namespace PERPUSTAKAAN.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BukuController : Controller
    {
        private readonly DABuku buku;
        public BukuController(PERPUSTAKAANContext _db)
        {
            buku = new DABuku(_db);
        }
        [HttpGet]
        public VMResponse GetAll()
        {
            return buku.GetAll();
        }
        [HttpGet("[action]")]
        public VMResponse GetById(int id)
        {
            return buku.GetById(id);
        }
        [HttpGet("[action]")]
        public VMResponse GetByRakId(int id)
        {
            return buku.GetByRakId(id);
        }
        [HttpPost]
        public VMResponse Create(VMBuku data)
        {
            return buku.Create(data);
        }
        [HttpPut]
        public VMResponse Update(VMBuku data)
        {
            return buku.Update(data);
        }
        [HttpDelete]
        public VMResponse Delete(int id, int userId)
        {
            return buku.Delete(id, userId);
        }
    }
}
