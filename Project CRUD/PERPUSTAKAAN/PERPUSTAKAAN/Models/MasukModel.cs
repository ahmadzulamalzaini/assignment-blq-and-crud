﻿using Newtonsoft.Json;
using PERPUSTAKAAN.ViewModel;
using System.Net;

namespace PERPUSTAKAAN.Models
{
	public class MasukModel
	{
		private string? jsonData;
		private HttpContent? content;
		private VMResponse? apiResponse;
		private readonly string? apiUrl;
		private readonly HttpClient httpClient;

		public MasukModel(IConfiguration _config)
		{
			apiUrl = _config["ApiUrl"];
			httpClient = new HttpClient();
		}
		public async Task<VMResponse> Masuk(string email, string password)
		{
			try
			{
				apiResponse = JsonConvert.DeserializeObject<VMResponse>(
					await httpClient.GetStringAsync(apiUrl + $"api/Masuk/?email={email}&password={password}"));
				if (apiResponse != null)
				{
					if (apiResponse.statusCode == HttpStatusCode.OK)
					{
						apiResponse.data = JsonConvert.DeserializeObject<VMPengguna?>(apiResponse.data.ToString());
					}
					else
					{
						VMPengguna emailData = new VMPengguna();
						emailData.Email = email;
						apiResponse.data = JsonConvert.SerializeObject(emailData);
						apiResponse.data = JsonConvert.DeserializeObject<VMPengguna?>(apiResponse.data.ToString());
					}
				}
				else
				{
					throw new Exception("Tidak bisa terhubung ke API user");
				}
			}
			catch (Exception ex)
			{
				apiResponse.message = ex.Message;
			}
			return apiResponse;
		}

	}
}
