﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PERPUSTAKAAN.DataModel
{
    public partial class PERPUSTAKAANContext : DbContext
    {
        public PERPUSTAKAANContext()
        {
        }

        public PERPUSTAKAANContext(DbContextOptions<PERPUSTAKAANContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Anggotum> Anggota { get; set; } = null!;
        public virtual DbSet<Buku> Bukus { get; set; } = null!;
        public virtual DbSet<Peminjaman> Peminjamen { get; set; } = null!;
        public virtual DbSet<Pengembalian> Pengembalians { get; set; } = null!;
        public virtual DbSet<Pengguna> Penggunas { get; set; } = null!;
        public virtual DbSet<Petuga> Petugas { get; set; } = null!;
        public virtual DbSet<Rak> Raks { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=localhost; Initial Catalog=PERPUSTAKAAN; User Id= sa; Password=P@ssw0rd; Integrated Security=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Anggotum>(entity =>
            {
                entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.JkAnggota).IsFixedLength();
            });

            modelBuilder.Entity<Buku>(entity =>
            {
                entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.KodeBuku).IsFixedLength();

                entity.Property(e => e.TahunPenerbit).IsFixedLength();
            });

            modelBuilder.Entity<Peminjaman>(entity =>
            {
                entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Pengembalian>(entity =>
            {
                entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Pengguna>(entity =>
            {
                entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Petuga>(entity =>
            {
                entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Rak>(entity =>
            {
                entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
