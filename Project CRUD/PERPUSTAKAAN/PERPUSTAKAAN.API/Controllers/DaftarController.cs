﻿using Microsoft.AspNetCore.Mvc;
using PERPUSTAKAAN.DataAccess;
using PERPUSTAKAAN.DataModel;
using PERPUSTAKAAN.ViewModel;

namespace PERPUSTAKAAN.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DaftarController : Controller
    {
        private DADaftar daftar;

        public DaftarController(PERPUSTAKAANContext _db)
        {
            daftar = new DADaftar(_db);
        }
        [HttpGet("[action]")]
        public VMResponse GetAllPetugas()
        {
            return daftar.GetAllPetugas();
        }
        [HttpGet("[action]")]
        public VMResponse GetAllAnggota()
        {
            return daftar.GetAllAnggota();
        }
		[HttpGet("[action]")]
		public VMResponse GetAllPengguna()
		{
			return daftar.GetAllPengguna();
		}
		[HttpGet("[action]")]
        public VMResponse GetPetugasById(int id)
        {
            return daftar.GetPetugasById(id);
        }
		[HttpGet("[action]")]
		public VMResponse GetPenggunaById(int id)
		{
			return daftar.GetPenggunaById(id);
		}
		[HttpGet("[action]")]
        public VMResponse GetAnggotaById(int id)
        {
            return daftar.GetAnggotaById(id);
        }

        [HttpPost("[action]")]
        public VMResponse DaftarPetugas(VMPetugas data)
        {
            return daftar.DaftarPetugas(data);
        }
        [HttpPost("[action]")]
        public VMResponse DaftarAnggota(VMAnggota data)
        {
            return daftar.DaftarAnggota(data);
        }
		[HttpPost("[action]")]
		public VMResponse Daftar(VMPengguna data)
		{
			return daftar.Daftar(data);
		}
		[HttpPut("[action]")]
        public VMResponse UpdatePetugas(VMPetugas data)
        {
            return daftar.UpdatePetugas(data);
        }
        [HttpPut("[action]")]
        public VMResponse UpdateAnggota(VMAnggota data)
        {
            return daftar.UpdateAnggota(data);
        }
		[HttpPut("[action]")]
		public VMResponse Update(VMPengguna data)
		{
			return daftar.Update(data);
		}
	}
}
