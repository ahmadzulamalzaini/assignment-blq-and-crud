﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_BLQ
{
    public class Soal
    {
        public void Soal1()
        {
            int uangAndi = 0;
            int jumlah = 0;
            int index = 0;
            List<int> kacaMata = new List<int>() { 500, 600, 700, 800 };
            List<int> baju = new List<int>() { 200, 400, 350 };
            List<int> sepatu = new List<int>() { 400, 350, 200, 300 };
            List<int> buku = new List<int>() { 100, 50, 150 };
            List<string> variasi = new List<string>();
            List<int> harga = new List<int>();
            List<int> hargaSort = new List<int>();
            List<int> idx = new List<int>();
            List<int> jumlahItem = new List<int>();

            Console.Write("Masukkan uang Andi: ");
            uangAndi = int.Parse(Console.ReadLine());

            foreach (int km in kacaMata)
            {
                foreach (int bj in baju)
                {
                    foreach (int s in sepatu)
                    {
                        foreach (int bk in buku)
                        {
                            jumlah = km + bj + s + bk;
                            if (jumlah <= uangAndi)
                            {
                                harga.Add(jumlah);
                                hargaSort.Add(jumlah);
                                string varianValue =
                                    $"Kacamata = {km}\n" +
                                    $"Baju     = {bj}\n" +
                                    $"Sepaju   = {s}\n" +
                                    $"Buku     = {bk}\n" +
                                    $"Total    = {jumlah}\n";
                                variasi.Add(varianValue);
                            }
                        }
                    }
                }
            }
            hargaSort.Sort();
            for (int i = 0; i < harga.Count; i++)
            {
                if (harga[i] == hargaSort[hargaSort.Count - 1])
                {
                    idx.Add(i);
                }
            }
            Console.WriteLine("Variasi barang yang bisa dibeli andi:");
            foreach (int i in idx)
            {
                Console.WriteLine(variasi[i]);
                int k = variasi[i].Count(c => c == '\n');
                jumlahItem.Add(k);
                Console.WriteLine(k);
            }

        }
        public void Soal2()
        {
            double durasiTelatA = 0, durasiTelatB = 0;
            int buku1 = 14, buku2 = 3, buku3 = 7, buku4 = 7;
            DateTime aStart = new DateTime(2016, 2, 28), aEnd = new DateTime(2016, 3, 7);

            DateTime bStart = new DateTime(2018, 4, 29), bEnd = new DateTime(2018, 3, 30);

            TimeSpan intervalA = aEnd - aStart, intervalB = bEnd - bStart;
            double waktuA = intervalA.TotalDays, waktuB = intervalB.TotalDays;

            //case a
            durasiTelatA += (waktuA > buku1) ? (waktuA - buku1) : 0;
            durasiTelatA += (waktuA > buku2) ? (waktuA - buku2) : 0;
            durasiTelatA += (waktuA > buku3) ? (waktuA - buku3) : 0;
            durasiTelatA += (waktuA > buku4) ? (waktuA - buku4) : 0;

            //case b
            durasiTelatB += (waktuB > buku1) ? (waktuB - buku1) : 0;
            durasiTelatB += (waktuB > buku2) ? (waktuB - buku2) : 0;
            durasiTelatB += (waktuB > buku3) ? (waktuB - buku3) : 0;
            durasiTelatB += (waktuB > buku4) ? (waktuB - buku4) : 0;

            Console.WriteLine($"Denda pada case A adalah {durasiTelatA * 100} rupiah");
            Console.WriteLine($"Denda pada case B adalah {durasiTelatB * 100} rupiah");
        }
        public void Soal3()
        {
            double lamaA = 0, lamaB = 0, lamaC = 0, lamaD = 0;
            double biayaA = 0, biayaB = 0, biayaC = 0, biayaD = 0;
            DateTime aStart = new DateTime(2019, 1, 27, 5, 00, 1), aEnd = new DateTime(2019, 1, 27, 17, 45, 3);
            DateTime bStart = new DateTime(2019, 1, 27, 7, 3, 59), bEnd = new DateTime(2019, 1, 27, 15, 30, 06);
            DateTime cStart = new DateTime(2019, 1, 27, 7, 5, 0), cEnd = new DateTime(2019, 1, 28, 00, 20, 21);
            DateTime dStart = new DateTime(2019, 1, 27, 11, 14, 23), dEnd = new DateTime(2019, 1, 27, 13, 20, 00);
            TimeSpan intervalA = aEnd - aStart, intervalB = bEnd - bStart, intervalC = cEnd - cStart, intervalD = dEnd - dStart;

            //case a
            if (intervalA.TotalSeconds <= 8 * 3600)
            {
                biayaA = Math.Ceiling(intervalA.TotalHours) * 1000;
                Console.WriteLine($"Biaya parkir A adalah {biayaA}");
            }
            else if (intervalA.TotalSeconds <= 24 * 3600)
            {
                biayaA = 8000;
                Console.WriteLine($"Biaya parkir A adalah {biayaA}");
            }
            else
            {
                biayaA = 15000 + (Math.Ceiling(intervalA.TotalHours) * 1000);
                Console.WriteLine($"Biaya parkir A adalah {biayaA}");
            }
            //case b
            if (intervalB.TotalSeconds <= 8 * 3600)
            {
                biayaB = Math.Ceiling(intervalB.TotalHours) * 1000;
                Console.WriteLine($"Biaya parkir B adalah {biayaB}");
            }
            else if (intervalB.TotalSeconds <= 24 * 3600)
            {
                biayaB = 8000;
                Console.WriteLine($"Biaya parkir B adalah {biayaB}");
            }
            else
            {
                biayaB = 15000 + (Math.Ceiling(intervalB.TotalHours) * 1000);
                Console.WriteLine($"Biaya parkir B adalah {biayaB}");
            }
            //case c
            if (intervalC.TotalSeconds <= 8 * 3600)
            {
                biayaC = Math.Ceiling(intervalC.TotalHours) * 1000;
                Console.WriteLine($"Biaya parkir C adalah {biayaC}");
            }
            else if (intervalC.TotalSeconds <= 24 * 3600)
            {
                biayaC = 8000;
                Console.WriteLine($"Biaya parkir C adalah {biayaC}");
            }
            else
            {
                biayaC = 15000 + (Math.Ceiling(intervalC.TotalHours) * 1000);
                Console.WriteLine($"Biaya parkir C adalah {biayaC}");
            }
            //case d
            if (intervalD.TotalSeconds <= 8 * 3600)
            {
                biayaD = Math.Ceiling(intervalD.TotalHours) * 1000;
                Console.WriteLine($"Biaya parkir D adalah {biayaD}");
            }
            else if (intervalD.TotalSeconds <= 24 * 3600)
            {
                biayaD = 8000;
                Console.WriteLine($"Biaya parkir D adalah {biayaD}");
            }
            else
            {
                biayaD = 15000 + (Math.Ceiling(intervalD.TotalHours) * 1000);
                Console.WriteLine($"Biaya parkir D adalah {biayaD}");
            }
        }
        public bool IsPrima(int nilai)
        {
            for (int i = 2; i < nilai; i++)
            {
                if (nilai % i == 0) return false;
            }
            if (nilai <= 1) return false;
            else return true;
        }
        public void Soal4()
        {
            Console.Write("Masukkan suatu bilangan: ");
            int nilai = int.Parse(Console.ReadLine());
            List<int> prima = new List<int>();
            for (int i = 0; i < int.MaxValue; i++)
            {
                if (IsPrima(i))
                {
                    prima.Add(i);
                    if(prima.Count == nilai)
                    {
                        break;
                    }
                }
            }
            Console.WriteLine();
            Console.WriteLine("Bilangan prima diantara bilangan tersebut adalah {0}", string.Join(" ", prima));
        }
        public void Soal5()
        {
            Console.Write("Masukkan banyak deret: ");
            int banyakDeret = int.Parse(Console.ReadLine());
            int fn1 = 1;
            int fn2 = 0;
            Console.Write("Deret Fibonacci: ");
            for (int i = 1; i <= banyakDeret; i++)
            {
                int fn = fn1 + fn2;
                fn2 = fn1;
                fn1 = fn;
                Console.Write(fn + " ");
            }
        }
        public void Soal6()
        {
            string kata = "";
            int check = 0;
            Console.Write("Masukkan kata: ");
            kata = Console.ReadLine();

            for (int i = 0; i < kata.Length; i++)
            {
                if (kata[i] != kata[kata.Length - 1 - i])
                {
                    check += 1;
                }
            }
            if (check == 0)
            {
                Console.WriteLine($"Kata {kata} adalah palindrom");
            }
            else
            {
                Console.WriteLine($"Kata {kata} bukan palindrom");

            }
        }
        public void Soal7()
        {
            List<int> deret = new List<int>() { 8, 7, 0, 2, 7, 1, 7, 6, 3, 0, 7, 1, 3, 4, 6, 1, 6, 4, 3 };
            double mean = 0, median = 0, modus = 0;
            double idx = 0;
            //mean
            mean = (deret.Sum() / deret.Count());

            //median
            deret.Sort();
            if (deret.Count % 2 != 0)
            {
                idx = (deret.Count + 1) * 0.5;
                median = deret[(int)idx];
            }
            else
            {
                idx = ((deret.Count * 0.5) + (deret.Count * 0.5) + 1) * 0.5;
                median = deret[(int)idx];
            }
            //modus
            var groups = deret.GroupBy(v => v);
            int count = groups.Max(g => g.Count());
            modus = groups.First(g => g.Count() == count).Key;

            Console.WriteLine($"Mean  :  {mean}");
            Console.WriteLine($"Median:  {median}");
            Console.WriteLine($"Modus :  {modus}");
        }
        public void Soal8()
        {
            List<int> deret = new List<int>() { 1, 2, 4, 7, 8, 6, 9 };
            int min = deret[0] + deret[1] + deret[2] + deret[3];
            int max = deret[0] + deret[1] + deret[2] + deret[3];

            for (int i = 0; i < deret.Count - 3; i++)
            {
                for (int j = i + 1; j < deret.Count - 2; j++)
                {
                    for (int k = j + 1; k < deret.Count - 1; k++)
                    {
                        for (int l = k + 1; l < deret.Count; l++)
                        {
                            int sum = deret[i] + deret[j] + deret[k] + deret[l];
                            min = (sum < min) ? sum : min;
                            max = (sum > max) ? sum : max;
                        }
                    }
                }
            }
            Console.WriteLine("Nilai minimal: " + min);
            Console.WriteLine("Nilai maksimal: " + max);
        }
        public void Soal9()
        {
            int n = 0;
            int awal = 0;
            Console.Write("Masukkan nilai N: ");
            n = int.Parse(Console.ReadLine());

            awal = n;
            Console.Write($"N = {n} --> ");
            for (int i = 0; i < n; i++)
            {
                Console.Write($" {awal}");
                awal += n;
            }

        }
        public void Soal10()
        {
            Console.Write("Masukkan kata: ");
            string kata = Console.ReadLine();
            string[] arrayKata = kata.Split(" ");
            List<char> listKata = new List<char>();

            for (int i = 0; i < arrayKata.Length; i++)
            {

                string kataa = arrayKata[i];
                listKata.Add(kataa[0]);
                for (int j = 0; j < 3; j++)
                {
                    listKata.Add('*');
                }
                listKata.Add(kataa[arrayKata[i].Length - 1]);
                listKata.Add(' ');
            }

            string outputString = string.Join("", listKata);
            Console.WriteLine(outputString);
        }
        public void Soal11()
        {
            string nama;
            char[] charNama = new char[0];

            Console.Write("Masukkan kata: ");
            nama = Console.ReadLine().ToLower();
            charNama = nama.ToCharArray();
            Console.WriteLine();
            for (int i = charNama.Length-1; i >= 0; i--)
            {
                for (int j = 1; j <= Math.Floor((double)charNama.Length * 0.5); j++)
                {
                    Console.Write("*");
                }
                Console.Write(charNama[i]);
                for (int j = 1; j <= Math.Floor((double)charNama.Length * 0.5); j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }
        public void Soal12()
        {
            List<int> bilangan = new List<int>() { 1, 2, 1, 3, 4, 7, 1, 1, 5, 6, 1, 8 };

            Console.WriteLine("Bilangan awal: {0}", string.Join(",", bilangan));

            for (int j = 1; j <= bilangan.Count; j++)
            {
                for (int i = 0; i < bilangan.Count - 1; i++)
                {
                    if (bilangan[i] > bilangan[i + 1])
                    {
                        bilangan.Insert(i, bilangan[i + 1]);
                        bilangan.RemoveAt(i + 2);

                    }
                }
            }
            Console.WriteLine("Hasil Sorting:  {0}", string.Join(",", bilangan));
        }
        public void Soal13()
        {
            DateTime waktu = new DateTime();
            int jam = 0, menit = 0;
            double sudut = 0;
            Console.Write("Masukkan jam(HH:mm): ");
            waktu = DateTime.Parse(Console.ReadLine());

            jam = waktu.Hour;
            menit = waktu.Minute;
            double j = (double)menit / 60;
            sudut = Math.Abs((((jam * 30) + (j * 30)) - (menit * 6)));

            Console.WriteLine($"Jam {waktu.ToString("HH:mm")} --> {sudut}");
        }
        public void Soal14()
        {
            List<int> list = new List<int>() { 3, 9, 0, 7, 1, 2, 4 };
            Console.Write("Masukkan N: ");
            int rotasi = int.Parse(Console.ReadLine());
            List<int> list2 = new List<int>();
            int count = list.Count;
            Console.Write($"N = {rotasi} --> ");
            for (int i = 1; i <= rotasi; i++)
            {
                int a = count - 1 + i;
                list.Insert(a, list[i - 1]);
            }
            for (int i = 0; i < rotasi; i++)
            {
                list.RemoveAt(0);
            }
            Console.Write(string.Join(",", list));
        }
        public void Soal15()
        {
            DateTime tLama = DateTime.Parse("03:40:44 PM");

            Console.WriteLine($"Format sebelum: {tLama.ToString("hh:mm:ss tt")}");
            Console.WriteLine($"Format sesudah: {tLama.ToString("HH:mm:ss")}");
        }
        public void Soal16()
        {
            int orang = 4, bayar = 0;
            double pajak = 0, service = 0, totalBiaya = 0, hargaPerOrang = 0;
            List<int> harga = new List<int>() { 42000, 50000, 30000, 70000, 30000 };
            Console.WriteLine("1. Tuna Sandwich\t\t42K (mengandung ikan)\r\n2. Spaghetti Carbonara\t\t50K \r\n\t3. Tea pitcher\t\t\t30K\r\n\t4. Pizza\t\t\t\t70K\r\n\t5. Salad\t\t\t\t30K\r\n");
            Console.Write("Kamu ingin pesan apa(masukkan nomor, pisah dg koma): ");
            string[] pesanan = Console.ReadLine().Split(",");
            List<int> listPesanan = new List<int>();

            foreach(string i in pesanan)
            {
                listPesanan.Add(int.Parse(i));
            }
            for(int i = 0;i <= harga.Count; i++)
            {
                if(i < listPesanan.Count)
                {
                    bayar += harga[listPesanan[i]-1];
                }
            }
            pajak = 0.1 * bayar;
            service = 0.05 * bayar;
            totalBiaya = bayar + pajak + service;
            if (pesanan.Contains("1"))
            {
                double tuna = (harga[0] + (harga[0] * 0.1) + (harga[1] * 0.05));
                hargaPerOrang = (totalBiaya - tuna) / 4;
                double harga3Orang = (tuna / 3) + hargaPerOrang;

                Console.Write("Ada yang alergi ikan? siapa namanya? ");
                string nama = Console.ReadLine();
                Console.WriteLine($"3 orang membayar {harga3Orang}");
                Console.WriteLine($"{nama} membayar {hargaPerOrang}");
            }
            else
            {
                Console.WriteLine($"Semuanya membayar {totalBiaya/4}");
            }
        }
        public void Soal17()
        {
            string kode;
            char[] arrayKode = new char[0];
            int level = 0, lembah = 0, gunung = 0;

            Console.Write("Masukkan Kode:");
            kode = Console.ReadLine().ToUpper();

            arrayKode = kode.ToCharArray();
            Console.WriteLine("Kode: {0}", string.Join(" ", arrayKode));
            for (int i = 0; i < arrayKode.Length; i++)
            {
                if (arrayKode[i] != ' ')
                {
                    level += (arrayKode[i] == 'N') ? 1 : -1;
                    gunung += (arrayKode[i] == 'T' && level == 0) ? 1 : 0;
                    lembah += (arrayKode[i] == 'N' && level == 0) ? 1 : 0;
                }

            }
            Console.WriteLine("Berapa kali pendaki melewati gunung: {0}", gunung);
            Console.WriteLine("Berapa kali pendaki melewati lembah: {0}", lembah);
        }
        public void Soal18()
        {

        }
        public void Soal19()
        {
            string huruf = "abcdefghijklmnopqrstuvwxyz", stringKata;
            string kata;
            char[] kataKata = new char[0];
            List<char> kataPangram = new List<char>();

            Console.Write("Masukkan kata: ");
            kata = Console.ReadLine().ToLower(); ;
            kataKata = kata.ToCharArray();
            Array.Sort(kataKata);

            for (int i = 1; i < kataKata.Length; i++)
            {
                if (kataKata[i - 1] != kataKata[i] && kataKata[i] != ' ')
                {
                    kataPangram.Add(kataKata[i]);
                }
            }
            stringKata = string.Join("", kataPangram);
            if (stringKata == huruf)
            {
                Console.WriteLine("Kalimat ini adalah pangram");
            }
            else
            {
                Console.WriteLine("Kalimat ini bukan pangram");
            }
        }
        public void Soal20()
        {
            string[] a, b;
            int jarakAwal = 0, jarakAkhir = 0;
            int jarakA = 0, jarakB = 0, jarakALama = 0, jarakBLama = 0;
            Console.WriteLine("Masukkan huruf B, K, atau G selama suit");
            Console.Write("Suit A(pisahkan dengan spasi): ");
            a = Console.ReadLine().ToUpper().Split(" ");
            Console.Write("Suit B(pisahkan dengan spasi): ");
            b = Console.ReadLine().ToUpper().Split(" ");
            Console.Write("Jarak awal: ");
            jarakAwal = int.Parse(Console.ReadLine());
            jarakB = jarakAwal;

            for (int i = 0; i < a.Length; i++)
            {
                jarakALama = jarakA;
                jarakBLama = jarakB;
                jarakA += ((a[i] == "B" && b[i] == "G") || (a[i] == "G" && b[i] == "K") || (a[i] == "K" && b[i] == "B")) ? 2 : -1;

                jarakB += ((b[i] == "B" && a[i] == "G") || (b[i] == "G" && a[i] == "K") || (b[i] == "K" && a[i] == "B")) ? 2 : -1;

                if(jarakA == jarakB)
                {
                    Console.Write((jarakA == jarakALama + 2)?"Pemain A ":"Pemain B ");
                    break;

                }
            }
            jarakAkhir = Math.Abs(jarakA - jarakB);
            if(jarakA == jarakB)
            {
                Console.Write("Menang");
            }
            else
            {
                Console.WriteLine("DRAW");
            }
        }
        public void Soal21()
        {
            string jawab, jalur, hasil = "";
            int lintasan = 0, st = 0, d = 0;
            Console.WriteLine("Rincian lintasan: \n* -> posisi awal" +
                "_ -> lintasan biasa\n" +
                "O -> lubang\n");
            Console.WriteLine("Lintasan:\n" +
                "1. *_ _ _ _ _ O _ _ _\n" +
                "2. *O _ _ _ _ O _ _ _");
            Console.Write("Pilih lintasan(1 atau 2): ");
            lintasan = int.Parse(Console.ReadLine());
            Console.Write("Masukkan jawaban(W or J):");
            jawab = Console.ReadLine().ToLower();

            if(lintasan == 1)
            {
                jalur = "_____O___";
            }
            else if(lintasan == 2)
            {
                jalur = "O____O___";
            }
            else
            {
                jalur = null;
                Console.WriteLine("Pilih jalur yang sesuai");
            }
            if(jalur != null)
            {
                for(int i = 0; i < int.MaxValue;i++)
                {
                    if(i < jawab.Length)
                    {
                        if (jawab[i] == 'w')
                        {
                            st += 1;d += 1;
                        }
                        else if (jawab[i] == 'j')
                        {
                            st -= 2;d += 3;
                        }

                        if(d > jalur.Length) {
                            hasil = "You Won";
                            break; }
                        if((st <= 0) || (jalur[d-1] =='O'))
                        {
                            hasil = "Failed";
                            break;
                        }
                        else
                        {
                            hasil = "You Won";
                        }
                    }
                }
            }
            if(d <= jalur.Length) { hasil = "Failed"; }
            Console.WriteLine(hasil);
        }
        public void Soal22()
        {
            List<int> laju = new List<int>() { 1, 1, 2, 3, 5, 8, 13 };
            List<int> lilin = new List<int>() { 3, 3, 9, 6, 7, 8, 23 };
            double waktuTerkecil = int.MaxValue;
            int panjangLilin = 0;
            for(int i = 0; i < laju.Count; i++)
            {
                double a = ((double)lilin[i] / (double)laju[i]);
                Console.WriteLine($"Lilin {i+1} meleleh dalam {a} detik");
                if(a < waktuTerkecil)
                {
                    waktuTerkecil = a;
                    panjangLilin = lilin[i];
                }
            }
            Console.WriteLine($"\nJadi lilin yang paling pertama meleleh adalah lilin yang panjangnya {panjangLilin} yaitu selama {waktuTerkecil} detik");
        }
    }
}
