﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PERPUSTAKAAN.DataModel
{
    [Table("Pengembalian")]
    public partial class Pengembalian
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("tanggal_pengembalian", TypeName = "datetime")]
        public DateTime TanggalPengembalian { get; set; }
        [Column("denda")]
        public int Denda { get; set; }
        [Column("id_buku")]
        public int IdBuku { get; set; }
        [Column("id_anggota")]
        public int IdAnggota { get; set; }
        [Column("id_petugas")]
        public int IdPetugas { get; set; }
        [Column("is_deleted")]
        public bool IsDeleted { get; set; }
        [Column("deleted_by")]
        public int? DeletedBy { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Column("create_by")]
        public int CreateBy { get; set; }
        [Column("create_date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("update_by")]
        public int? UpdateBy { get; set; }
        [Column("update_date", TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
    }
}
