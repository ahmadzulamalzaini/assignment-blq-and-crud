﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NuGet.Common;
using PERPUSTAKAAN.Models;
using PERPUSTAKAAN.ViewModel;
using System.Net;

namespace PERPUSTAKAAN.Controllers
{
	public class DaftarController : Controller
	{
		private DaftarModel daftar;
		private VMResponse? response;
		string JsonData;
		public DaftarController(IConfiguration _config)
		{
			daftar = new DaftarModel(_config);
		}
		public IActionResult Index()
		{
			return View();
		}
		public IActionResult DataDiri(string role,VMPengguna data)
		{
			ViewBag.Email = data.Email;
			ViewBag.Password = data.Password;
			ViewBag.Role = role;
			return View();
		}
        public IActionResult DataPetugas(string role, VMPengguna data)
        {
            ViewBag.Email = data.Email;
            ViewBag.Password = data.Password;
            ViewBag.Role = role;
            return View();
        }
		[HttpPost]
		public async Task<VMResponse> Daftar(VMPengguna dataPengguna, VMPetugas dataPetugas, VMAnggota dataAnggota, string role)
		{
			if(role == "anggota")
			{
                VMResponse? agResponse = await daftar.DaftarAnggota(dataAnggota);
				if(agResponse.statusCode == HttpStatusCode.Created)
				{
					JsonData = JsonConvert.SerializeObject(agResponse.data);
					VMAnggota dataAg = new VMAnggota();
					dataAg = JsonConvert.DeserializeObject<VMAnggota>(JsonData);
					int idAnggota = dataAg.Id;
					dataAnggota.Id = idAnggota;
					dataAnggota.KodeAnggota = Convert.ToString(idAnggota);
					VMResponse? upResponse = await daftar.UpdateAnggota(dataAnggota);

					dataPengguna.IdAnggota = idAnggota;
					response = await daftar.Daftar(dataPengguna);

                    JsonData = JsonConvert.SerializeObject(response.data);
                    VMPengguna dataPg = new VMPengguna();
                    dataPg = JsonConvert.DeserializeObject<VMPengguna>(JsonData);
                    int idPg = dataPg.Id;
					dataPengguna.Id = idPg;
                    upResponse = await daftar.Update(dataPengguna);
				}
				
			}
			else if(role == "petugas")
			{
				VMResponse? agResponse = await daftar.DaftarPetugas(dataPetugas);
                if (agResponse.statusCode == HttpStatusCode.Created)
                {
                    JsonData = JsonConvert.SerializeObject(agResponse.data);
                    VMPetugas dataAg = new VMPetugas();
                    dataAg = JsonConvert.DeserializeObject<VMPetugas>(JsonData);
                    int idPetugas = dataAg.Id;
                    dataPetugas.Id = idPetugas;
                    VMResponse? upResponse = await daftar.UpdatePetugas(dataPetugas);

                    dataPengguna.IdPetugas = idPetugas;
                    response = await daftar.Daftar(dataPengguna);

                    JsonData = JsonConvert.SerializeObject(response.data);
                    VMPengguna dataPg = new VMPengguna();
                    dataPg = JsonConvert.DeserializeObject<VMPengguna>(JsonData);
                    int idPg = dataPg.Id;
                    dataPengguna.Id = idPg;
                    upResponse = await daftar.Update(dataPengguna);
                }
            }
			if(response.statusCode == HttpStatusCode.Created)
			{
                HttpContext.Session.SetString("infoMsg", "Pendaftaran berhasil");
            }
            else
            {
                HttpContext.Session.SetString("errMsg", "Pendaftaran gagal");
            }
            return response;
		}
    }
}
