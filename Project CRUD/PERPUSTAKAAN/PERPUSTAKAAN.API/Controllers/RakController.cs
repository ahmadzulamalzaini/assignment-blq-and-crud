﻿using Microsoft.AspNetCore.Mvc;
using PERPUSTAKAAN.DataAccess;
using PERPUSTAKAAN.DataModel;
using PERPUSTAKAAN.ViewModel;

namespace PERPUSTAKAAN.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RakController : Controller
    {
        private readonly DARak rak;
        public RakController(PERPUSTAKAANContext _db)
        {
            rak = new DARak(_db);
        }
        [HttpGet]
        public VMResponse GetAll()
        {
            return rak.GetAll();
        }        
        [HttpGet("[action]")]
        public VMResponse GetById(int id)
        {
            return rak.GetsById(id);
        }
        [HttpPost]
        public VMResponse Create(VMRak data)
        {
            return rak.Create(data);
        }
        [HttpPut]
        public VMResponse Update(VMRak data)
        {
            return rak.Update(data);
        }
        [HttpDelete]
        public VMResponse Delete(int id, int userId)
        {
            return rak.Delete(id, userId);
        }
    }
}
